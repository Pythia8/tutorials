#!/bin/bash

# Grab the Pythia source and build the full Python interface.
if [ ! -f pythia8311.tgz ]; then
    wget https://pythia.org/download/pythia83/pythia8311.tgz
    tar -xzvf pythia8311.tgz && rm pythia8311.tgz
    cp configure pythia8311/configure
    cd pythia8311/plugins/python
    ./generate --full
    cd - && tar -czvf pythia8311.tgz pythia8311
    rm -rf pythia8311
fi

# Build the Docker container.
docker build --network=host -t pythia8/tutorials:hsf23 .
