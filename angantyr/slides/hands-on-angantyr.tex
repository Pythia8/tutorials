\documentclass[aspectratio=169]{beamer}

%% To read the theme from specific folder
\makeatletter
  \def\beamer@calltheme#1#2#3{%
    \def\beamer@themelist{#2}
    \@for\beamer@themename:=\beamer@themelist\do
    {\usepackage[{#1}]{\beamer@themelocation/#3\beamer@themename}}}

  \def\usefolder#1{
    \def\beamer@themelocation{#1}
  }
  \def\beamer@themelocation{}

\usefolder{Theme}
\usetheme{metropolis}           % Use metropolis theme
%\metroset{sectionpage=none, progressbar=frametitle}
%\metroset{sectionpage=none, progressbar=frametitle, block=fill}
\metroset{progressbar=frametitle}

\usepackage{mdframed}% http://ctan.org/pkg/mdframed
\usepackage{fontspec}
\usepackage{xcolor}
\usepackage{graphics}
\usepackage{array}
\usepackage{multirow}
\usepackage{appendixnumberbeamer}
\usepackage{tikz}

\setmainfont[ Path = Fonts/, UprightFont= *-Light, BoldFont= *-Regular, ItalicFont= *-LightItalic, BoldItalicFont=*-Italic]{FiraSans}%
\setsansfont[ Path = Fonts/, UprightFont= *-Light, ItalicFont=*-LightItalic, BoldFont=*-Regular, BoldItalicFont=*-Italic]{FiraSans}%

\definecolor{JYUorange}{RGB}{251,65,64}
\definecolor{JYUblue}{RGB}{15,43,77}

\definecolor{TUred}{RGB}{165,30,55}
\definecolor{LundBlue}{RGB}{0,0,128}
\definecolor{DarkGreen}{RGB}{50,93,61}
\definecolor{LightGreen}{RGB}{100,186,122}
\definecolor{DarkBlue}{RGB}{0,0,139}
\definecolor{DarkRed}{RGB}{139,0,0}
\setbeamercolor{structure}{fg=JYUorange,bg=white}
\setbeamercolor{progress bar}{fg=JYUorange,bg=white}
%\setbeamercolor{background canvas}{bg=white!98!black}
\setbeamercolor{background canvas}{bg=white}

\setbeamercolor{palette primary}{bg=JYUblue}


%\setbeamertemplate{frame footer}{My custom footer}

\newcommand{\ra}{${\textcolor{structure}\Rightarrow}$}
\newcommand{\ee}{$\mathsf{e}^+\mathsf{e}^-$}
\newcommand{\ep}{$\mathsf{e}\mathsf{p}$}
\newcommand{\pp}{$\mathsf{p}\mathsf{p}$}
\newcommand{\Pom}{I$\!$P}
\newcommand{\Po}{I\hspace{-1pt}P}

\usepackage{scalerel,stackengine}
\newcommand\equalhat{\mathrel{\stackon[1.5pt]{=}{\stretchto{%
    \scalerel*[\widthof{=}]{\wedge}{\rule{1ex}{3ex}}}{0.5ex}}}}

%% Re-position the logos on front page.
\setbeamertemplate{title graphic}{
  \vbox to 0pt {
    \vspace*{13 em}
    \inserttitlegraphic%
  }%
  \nointerlineskip%
}
\setbeamertemplate{itemize/enumerate subbody begin}{\normalsize}
\setbeamertemplate{itemize/enumerate subsubbody begin}{\normalsize}
\setlength{\leftmargini}{1.5em}
\setlength{\leftmarginii}{1.5em}
\setlength{\leftmarginiii}{1.5em}


\title{Hands-on Pythia 8.3 features: Particle production with Angantyr}
\date{Last updated June 2021}
\author{\textcolor{structure}{Christian Bierlich}}
%\institute{University of Jyväskylä}
\subtitle{PYTHIA features: Angantyr}

\titlegraphic{
\vspace{0.7cm}
\begin{columns}
\column{0.3\textwidth}\mbox{}
\column{0.5\textwidth}
\includegraphics[height=2.5cm]{figures/pythia-logo.png}
\end{columns}
}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Angantyr: Overview}
\vspace{18pt}
\begin{columns}%[T]
\column{0.55\textwidth}
\textcolor{structure}{The extension of the \textsc{Pythia} MPI model for heavy ion collisions.}
\begin{itemize}
\item[1.] A Glauber model determines which nucleons will interact (be \emph{wounded}). Own total cross section model, automatic fits to SaS.
\item[2.] Sub-collisions can either be \emph{absorptive} or \emph{diffractive}. The absorptive ones are the tricky part.
\item[3.] Signal processes etc. can be specified as usual.
\end{itemize}

\column{0.4\textwidth}
\includegraphics[width=1.2\textwidth]{figures/pPb-cartoon.jpg}\\
\end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Tell me more about absorptive collisions...}
\begin{columns}
\column{0.4\textwidth}
	\begin{itemize}
		\item Emission $F(\eta)$ per wounded nucleon $\rightarrow \frac{\mathrm{d}N}{\mathrm{d}\eta} = n_t F(\eta) + n_p F(-\eta)$.
		\item $F(\eta)$ modelled with even gaps in rapidity, as diffraction.
		\item Tuned to reproduce pp in the $n_t = n_p = 1$ case.
		\item No tunable parameters for $AA$ -- though some freedom in choices along the way.
	\end{itemize}
\column{0.57\textwidth}
	 \begin{tikzpicture}[every node/.style={scale=0.5}, align=center]
		 \draw [->] (0,0) node[below]{Projectile} -> (6,0) node[below]{Target~~\scalebox{1.5}{$\eta$}};
		 \draw [->] (3,0) -> (3,3) node[left]{\scalebox{1.5}{$\frac{\mathrm{d}N}{\mathrm{d}\eta}$}};
		 \pause
		 \draw (1,0) -> (5,0.5) node[right]{target wounded nucleon};
		 \pause
		 \draw (1, 0.5) node[left]{projectile wounded nucleon} -> (5,0);
		 \pause
		 \draw [red,dashed] (1, 0.5) -> (5, 0.5) node[above]{\textcolor{structure}{pp collision}};
		 \pause
		 \draw (1, 0.) -> (5, 1.0);
		 \draw (1, 0.) -> (5, 1.5);
		 \draw (1, 0.) -> (5, 2.0);
		 \draw (1, 0.) -> (5, 2.5);
		 \pause
		 \draw [red,dashed] (1, 0.5) -> (5, 2.5) node[right]{\textcolor{structure}{p$A$ collision}};
		 \pause
		 \draw (1, 1.0) -> (5, 0.);
		 \draw (1, 1.5) -> (5, 0.);
		 \draw (1, 2.0) -> (5, 0.);
		 \draw (1, 2.5) -> (5, 0.);
		 \draw [red,dashed] (1, 2.5) node[above]{\textcolor{structure}{$AA$ collision}} -> (5, 2.5);
 	\end{tikzpicture}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Exercise I: Multiplicity in p-Pb collisions at LHC}

%\vspace{18pt}
\begin{columns}%[T]
\column{0.6\textwidth}
\begin{itemize}
\item Use provided \texttt{test70.cc} to generate p-Pb collisions at LHC energies.
\item Add a figure with charged multiplicity at mid-eta scaled
by the number of absorptive and diffractive wounded nucleon pairs. 
\item The number of wounded nucleons are part of the \texttt{HIInfo} object. See \texttt{main113.cc} for inspiration.
\item Study the first part of the output in terminal.
\begin{itemize}
\item How well are the cross sections reproduced?
\end{itemize}
\item \textcolor{structure}{Plot the distributions using the generated Python code.}
\end{itemize}

\column{0.35\textwidth}
\textcolor{structure}{Expected result:}
	\includegraphics[width=0.95\textwidth]{figures/ppb1.pdf}
	\includegraphics[width=0.95\textwidth]{figures/ppb2.pdf}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Exercise II: Au-Au collisions at RHIC}
\begin{columns}%[T]
\column{0.6\textwidth}
\begin{itemize}
\item Generate the same figures for Au-Au collisions at $\sqrt{s_\mathrm{NN}} = 200$ GeV.
\item (Bonus: Make an additional histogram plotting the number of participant nucleons.)
\item Some things to consider:
\begin{itemize}
	\item Changing beam type and energy -- find help in the online manual.
	\item The cross sections change from 5 TeV to 200 GeV. How to make sure that this is taken care of? Hint: study the output.
\end{itemize}
\end{itemize}

\column{0.35\textwidth}
\\
	\textcolor{structure}{Expected result:}
	\includegraphics[width=0.95\textwidth]{figures/auau1.pdf}
	\includegraphics[width=0.95\textwidth]{figures/auau2.pdf}
\end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Exercise III: Pb-Pb collisions}

\begin{columns}%[T]
\column{0.6\textwidth}
%\textcolor{structure}{
\begin{itemize}
\item Generate the same figures for Pb-Pb collisions at $\sqrt{s_\mathrm{NN}} = 200$ GeV.
\begin{itemize}
	\item What happens to the two multiplicity figures when changing beam types?
	\item If you generated the bonus plot, compare its behaviour to the centrality measure.
	\item Why is there a difference in top plot from AA to pA collisions? (careful, trick question!)
	\item Generating 10k events will take time. Consider reducing the number, and just calculate averages.
\end{itemize}
\end{itemize}

\column{0.35\textwidth}
\\
	\textcolor{structure}{Expected result:}
	\includegraphics[width=0.95\textwidth]{figures/pbpb1.pdf}
	\includegraphics[width=0.95\textwidth]{figures/pbpb2.pdf}
\end{columns}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Bonus: Centrality dependence}
	\begin{itemize}
		\item If you need more to do, you can try to contruct observables like they do in heavy ion experiments.
		\item All observables are binned versus percentiles of the ``centrality observable''.
		\item Contruct two histograms with total multiplicity, one for 0-10\% most central, one for 10-20\%.
		\item The best way is a two-pass, where you generate the centrality observable first, use it to calculate cuts, and then run again.
		\item This procedure is automated in Rivet (see next slide).
	\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Bonus: Expected results}
	\begin{itemize}
		\item Taken from the Rivet analyses: \texttt{ATLAS\_PBPB\_CENTRALITY}, \texttt{ALICE\_2013\_I1225979} and \texttt{ALICE\_2016\_I1394676}.
		\item Study the Rivet documentation for specific instructions.
	\end{itemize}
	\begin{center}
	  \includegraphics[width=0.45\textwidth]{figures/sumETFwd.pdf}
	  \includegraphics[width=0.45\textwidth]{figures/combine5.pdf}
	\end{center}
\end{frame}

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


