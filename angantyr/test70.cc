
// Main program to for Angantyr tutorial.

#include "Pythia8/Pythia.h"
#include "Pythia8/HeavyIons.h"

using namespace Pythia8;

//==========================================================================

int main() {

  // Generator.
  Pythia pythia;

  // Decrease the output.
  pythia.readString("Init:showChangedSettings = off");
  pythia.readString("Init:showChangedParticleData = off");
  pythia.readString("Next:numberCount = 0");
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 2");
  pythia.readString("Next:numberShowEvent = 2");

  // Beam parameters for p-Pb collisions at LHC, 5TeV.
  pythia.readString("Beams:frameType = 2");
  pythia.readString("Beams:idA = 2212");
  pythia.readString("Beams:idB = 1000822080");
  pythia.readString("Beams:eA = 4000.");
  pythia.readString("Beams:eB = 1570.");

  // Initialize the Angantyr model to fit the total and semi-includive
  // cross sections in Pythia within some tolerance.
  // These parameters work for 5 TeV.
  pythia.readString("HeavyIon:SigFitErr = "
                    "0.02,0.02,0.1,0.05,0.05,0.0,0.1,0.0");
  // These parameters are suitable for sqrt(S_NN)=5TeV
  pythia.readString("HeavyIon:SigFitDefPar = "
                    "17.24,2.15,0.33,0.0,0.0,0.0,0.0,0.0");

  // A simple genetic algorithm is run for this many generations 
  // to fit the parameters. Change if you change COM energy.
  pythia.readString("HeavyIon:SigFitNGen = 0");

  // Initialize the generator.
  pythia.init();

  // Histogram for changed particles at mid-eta.
  Hist hNCh("charged particles", 10, 0, 25);

  // Histogram for charged particles forward and
  // backward (centrality measure).
  Hist hNFwd("charged particles 4 < |\\eta| < 5", 100, 0, 600);  
 
  // Number of events per run.
  int nEvent = 10000;
  double sow = 0.;
  // Begin event loop. Skip if fails.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    // Generate next event.
    if (!pythia.next()) continue;

    // Event weight.
    double weight = pythia.info.weight();
    sow += weight;
    // Number of participants.
    int np = 1; // <-- this should be the number of participants. Use main113 for inspiration.
    
    // Number of charged particles per event.
    int nCharged = 0;
    int nFwd = 0; 
    // Loop over event record and find charged final state particles.
    for (int i = 0; i < pythia.event.size(); ++i){
      Particle& p = pythia.event[i];
	// Calculate charged-particle multiplicity at mid-rapidity.
      if (p.isFinal() && p.isCharged() && p.isHadron()) {
        if (abs(p.eta()) < 1.0) ++nCharged;
	if (abs(p.eta()) < 5 && abs(p.eta()) > 4) ++nFwd;
      }
    }
    // Fill histograms.
    if (np > 0) {
      hNCh.fill(nCharged/np, weight); // <-- note normalization.
      hNFwd.fill(nFwd, weight);
    }
  }

  // Show statistics.
  pythia.stat();

  // Make Python plots.
  hNCh *= 1./sow;
  hNFwd.normalize();
  HistPlot hpl("test70plot");
  hpl.plotFrame("test07plot", hNCh, "Charged particles per participant pair", 
    "$dN_{ch}/d\\eta|_{\\eta = 0}$",
    "$(1/N_{\\mathrm{event}}) 2\\mathrm{d}N / \\mathrm{d}\\eta/N_{\\mathrm{part}}$",
    "-", "p-Pb", true);
  hpl.plotFrame("test07plot", hNFwd, "Centrality measure", 
    "$N_{\\mathrm{ch}}~4 < |\\eta| < 5$",
    "$P(N)$",
    "-", "p-Pb", true);
  // Done.
  return 0;
}
