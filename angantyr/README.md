# Event generation for Heavy Ion collisions

In this tutorial you will learn to generate heavy ion collisions
with the default heavy ion model in PYTHIA, called Angantyr.

Prerequisites:
- Basic knowledge about PYTHIA, ie. from going through
  the introductory worksheet.
- Minimal familiarity with matplotlib.
- For the extra assignment: Familiarity with Rivet. The rest
  of the tutorial can be completed without this.

## Instructions

Detailed instructions are in the slideshow in the slides/ directory.

The test70 program can be compiled with a normal Pythia Makefile, 
so you can either move it to your examples/ directory, or simply
replace Makefile.inc with your own Makefile.inc.

The program will generate a `.py` file containing your figures. Display
the figures by running the generated code with `python`.

## The slides

The introductory slides should be compiled with `xelatex`.

## References

The main physics reference for the Angantyr model is:
Bierlich et al.: JHEP 10 (2018) 134, arXiv: 1806.10820 [hep-ph]

Contact: Christian Bierlich <christian.bierlich@thep.lu.se>

