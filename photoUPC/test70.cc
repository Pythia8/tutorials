
// Main program to generate photoproduction events in HERA

#include "Pythia8/Pythia.h"

using namespace Pythia8;

//==========================================================================

int main() {

  // Generator.
  Pythia pythia;

  // Decrease the output.
  pythia.readString("Init:showChangedSettings = off");
  pythia.readString("Init:showChangedParticleData = off");
  pythia.readString("Next:numberCount = 0");
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 2");
  pythia.readString("Next:numberShowEvent = 2");

  // Beam parameters for photoproduction at HERA.
  pythia.readString("Beams:frameType = 2");
  pythia.readString("Beams:idA = -11");
  pythia.readString("Beams:idB = 2212");
  pythia.readString("Beams:eA = 27.5");
  pythia.readString("Beams:eB = 820.");
  pythia.readString("PDF:beamA2gamma = on");

  // Cuts on photon virtuality, invariant mass of gamma-hadron pair and pTHat.
  pythia.readString("Photon:Q2max = 1.0");
  pythia.readString("Photon:Wmin  = 100.0");
  pythia.readString("PhaseSpace:pTHatMin = 10.");

  // For photon-proton increase pT0Ref (for better agreement with HERA data).
  // Photon-photon has a new default pT0 parametrization tuned to LEP data.
  pythia.readString("MultipartonInteractions:pT0Ref = 3.00");
  
  pythia.readString("PhotonParton:all = on"); // Direct
  // pythia.readString("HardQCD:all = on");      // Resolved

  // Initialize the generator.
  pythia.init();

  // Book histogram for multiplicity
  Hist multiplicity("Charged-particle multiplicity", 100, -0.5, 199.5);
  
  // Number of events per run, 100k < 1 min with MBP for HERA, 10 min for LHC.
  int nEvent = 100000;

  // Begin event loop. Skip if fails.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    // Generate next event.
    if (!pythia.next()) continue;

    // Possible event weights.
    double weight = pythia.info.weight();

    // Number of charged particles per event.
    int nCharged = 0;
    
    // Loop over event record and find charged final state particles.
    for (int i = 0; i < pythia.event.size(); ++i){
      
	// Calculate charged-particle multiplicity.
      if ( pythia.event[i].isFinal() && pythia.event[i].isCharged() )
	++nCharged; 
    }

    // Fill in histogram.
    multiplicity.fill(nCharged, weight);

  }
  
  // Show statistics after each run.
  pythia.stat();

  // Normalize with number of events.
  multiplicity *= 1. / pythia.info.weightSum();
  
  // Print histogram and write to file for plotting.
  cout << multiplicity;
  multiplicity.table("nCH_epHERA_direct_pT10.dat");
  // multiplicity.table("nCH_epHERA_resolved_pT10.dat");
  
  // Done.
  return 0;
}
