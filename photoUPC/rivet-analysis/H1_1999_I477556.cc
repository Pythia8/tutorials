// -*- C++ -*-
#include "Rivet/Analysis.hh"
//#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
//#include "Rivet/Projections/IdentifiedFinalState.hh"

namespace Rivet {


  /// @brief H1 charged particle photoproduction
  ///
  /// @author Ilkka Helenius
  class H1_1999_I477556 : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(H1_1999_I477556);

    /// @name Analysis methods
    //@{

    // Book projections and histograms
    void init() {

      // Define acceptance
      const FinalState fs(Cuts::abseta < 1 && Cuts::pT > 2*GeV);
      const ChargedFinalState cfs(fs);
      declare(cfs, "CFS");

      // Book histograms
      book(_h_ch_pT, 1, 1, 1);
      book(_h_ch_eta1, 2, 1, 1);
      book(_h_ch_eta2, 2, 1, 2);

    }

    // Do the analysis
    void analyze(const Event& event) {

      // Find the charged final state particles
      const Particles& particles = apply<FinalState>(event, "CFS").particles();

      // Fill histograms. Convert from dsigma/dpt to dsigma/dpt^2 by assigning
      // a weight, dsigma/deta with two different pT cuts
      // foreach (const Particle& p, particles) {
      for (const Particle& p : particles) {
        _h_ch_pT->fill(p.pt(), 1.0 / (2. * 2. * p.pt() ));
        _h_ch_eta1->fill(p.eta());
        if (p.pt() > 3.) _h_ch_eta2->fill(p.eta());
      }
    }

    // Finalize
    void finalize() {
      const double norm = crossSection()/nanobarn/sumOfWeights();
      scale(_h_ch_pT, norm);
      scale(_h_ch_eta1, norm);
      scale(_h_ch_eta2, norm);
    }

    //@}


  private:

    /// @name Histograms
    //@{
    Histo1DPtr _h_ch_pT, _h_ch_eta1, _h_ch_eta2;
    //@}

  };

  DECLARE_RIVET_PLUGIN(H1_1999_I477556);

}
