#!/bin/bash

# Grab the Pythia source and build the full Python interface.
if [ ! -f pythia8307.tgz ]; then
    wget https://pythia.org/download/pythia83/pythia8307.tgz
    tar -xzvf pythia8307.tgz && rm pythia8307.tgz 
    cd pythia8307/plugins/python
    ./generate --full
    cd - && tar -czvf pythia8307.tgz pythia8307
    rm -rf pythia8307
fi

# Build the Docker container.
docker build --network=host -t pythia8/tutorials:cteq22 .
