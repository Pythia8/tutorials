# Pythia Tutorial

Contact: Leif Gellersen and Phil Ilten

## Getting started
You can download the Docker container for the tutorial in advance using the following command.
```
  docker pull pythia8/tutorials:cteq22
```
To start up the tutorial, run the following.
```
./run.sh
```
This will start a Jupyter notebook server, and create the folder `tutorial`. Please follow the instructions in `tutorial/worksheet8307.ipynb`. This worksheet is based on Pythia's Python interface.

## There's more
If you'd like to work with C++, you can either follow the instructions in `tutorial/worksheet8300.pdf` to install Pythia locally, or run the container interactively with the following.
```
./run.sh bash
```
This will start a `bash` shell within the Docker container, where Pythia is already installed, and the examples can be compiled.
