import matplotlib.pyplot as plt
import numpy as np

plt.rc('text', usetex=True)
plt.rc('font', family='serif',size=14)

x1,y1 = np.loadtxt("mTerr-default.dat", unpack = True)
#x2,y2 = np.loadtxt("mTerr-topHook.dat", unpack = True)
#x3,y3 = np.loadtxt("mTerr-vincia.dat", unpack = True)
#x4,y4 = np.loadtxt("mTerr-vincia-nonIRD.dat", unpack = True)

plt.step(x1, y1, color='C0', where='mid', label='Pythia (def)')
#plt.step(x2, y2, color='C1', where='mid', label='Pythia (top hook)')
#plt.step(x3, y3, color='C2', where='mid', label='Vincia')
#plt.step(x4, y4, color='C3', where='mid', label='Vincia (non-IRD)')

#plt.yscale('log', nonpositive='clip')
plt.xlim(xmin=-15., xmax=10.)
plt.ylim(ymin=0., ymax=0.1)

plt.xlabel(r"Reconstructed $\Delta m_{\mathrm{t}}$ [GeV]")
plt.ylabel(r"Prob($\Delta m$) [per GeV]")

plt.legend(frameon=False, numpoints=1, loc='upper left')
plt.savefig("deltaMT.pdf", bbox_inches='tight')

