#include "Pythia8/Pythia.h"
#include <ctime>

using namespace Pythia8;

int main() {

  // Input file.
  string cmndFile = "testMerging.cmnd";

  // Generator.
  Pythia pythia;
  pythia.readFile(cmndFile);

  // Extract number of events and max number of jets in merging.
  int nEvent = pythia.mode("Main:numberOfEvents");
  int nMerge = pythia.mode("Merging:nJetMax");

  // Merged total cross section.
  double  sigmaTot = 0.;
  map<int, double> sigmaSample;

  // Loop over subruns with varying number of jets.
  for (int iMerge(0); iMerge<=nMerge; ++iMerge) {
    // Initialise cross section.
    sigmaSample[iMerge] = 0.;

    // Read in name of LHE file for current subrun and initialize.
    pythia.readFile(cmndFile, iMerge);
    pythia.init();

    // Print a well-visible message at which multiplicity we are.
    cout << "\n *--------  Merging Info  -----------------------------*\n"
         << " |                                                     |\n"
         << " | Starting event loop for "
         << (iMerge>0?num2str((int)iMerge,1)+"-jet sample":"Born sample ")
         << "                |\n"
         << " |                                                     |\n"
         << " *-----------------------------------------------------*\n";

    // Event generation loop.
    auto start = std::clock();
    for (int iEvent(0); iEvent<nEvent; ++iEvent) {

      // Generate next event. Break out of event loop if at end of LHE file.
      if (!pythia.next()) {
        if ( pythia.info.atEndOfFile() ) break;
        else continue;
      }

      // Get CKKW-L weight of current event.
      double evtweight = pythia.info.weight();
      double weight    = pythia.info.mergingWeight();

      weight              *= evtweight;
      sigmaSample[iMerge] += weight;
    }
    auto stop = std::clock();
    // Print CPU time.
    cout << "\n CPU time for this multiplicity: "
         << (stop-start)/CLOCKS_PER_SEC << " s" << endl;

    // Print cross section and histograms for current subrun.
    pythia.stat();

    // Sum up merged cross section of current run.
    sigmaSample[iMerge] *= pythia.info.sigmaGen()
      / double(pythia.info.nAccepted());
    sigmaTot            += sigmaSample[iMerge];
  }

  // Print cross section information.
  cout << "\n *--------  XSec Summary  ----------------*\n"
       << " |                                        |\n"
       << " | Exclusive cross sections:              |\n";
  for (auto it=sigmaSample.begin(); it!=sigmaSample.end(); ++it) {
    if (it->first==0) cout << " |      Born:  ";
    else cout << " |     " << it->first << "-jet:  ";
    cout << setw(8) << scientific << setprecision(6)
         << it->second << " mb            |\n";
  }
  cout << " |                                        |\n"
       << " | CKKW-L merged inclusive cross section: |\n"
       << " |             " << setw(8) << scientific << setprecision(6)
       << sigmaTot << " mb            |\n"
       << " |                                        |\n"
       << " *----------------------------------------*\n\n";

  // Done
  return 0;

}
