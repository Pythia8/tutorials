# Pythia tutorial for MCnet school in Vietnam 2019

This Pythia tutorial is based on a variation of the standard worksheet, and was designed to work with Pythia version 8.243. On top of the usual content, it includes a section on shower uncertainties. Access the instructions by cloning this tutorial repository and opening worksheet/index.html in your browser. Beware that the bitbucket repository mentioned there is no longer available. Instead, you find the necessary files in the material folder of this repository.
